package com.company;

abstract class Figure {
    private int centrx;
    private int centry;
    public Figure (int centrx,int centry)
    {
        this.centrx = centrx;
        this.centry = centry;
    }
    public abstract int square();
    public int getQuadrant(){
        if (centrx>0 && centry>0){
            return 1;
        }
        else if (centrx<0 && centry>0){
            return 2;
        }
        else if (centrx<0 && centry<0){
            return 3;
        }
        else {
            return 4;
        }
    }
    public int getCentrx(){
        return this.centrx;
    }
    public int getCentry(){
        return this.centry;
    }

}
public class App
{
    static void main(String[] args)
    {
        Figure [] jojo = new Figure [5];
        for (int i = 0; i<5; i++)
        {
            if ((int) (Math.random()*10)<5)
            {
                Circle circ = new Circle((int) (Math.random()*20)-10,(int) (Math.random()*20)-10,(int) (Math.random()*10));
                System.out.println("Фигура Круг координаты центра x: "+circ.getCentrx()+" y: "+circ.getCentry()+" Радиус: "+ circ.getRadius()+" Четверть: "+ circ.getQuadrant()+ " Площадь: "+circ.square());
                jojo[i]= circ;
            }
            else
            {
                Rectingle rec = new Rectingle((int) (Math.random()*20)-10,(int) (Math.random()*20)-10,(int) (Math.random()*20),(int) (Math.random()*20));
                System.out.println("Фигура Прямоугольник координаты центра x: "+rec.getCentrx()+" y: "+rec.getCentry()+" Ширина: "+ rec.getWidth()+" Высота: "+rec.getHeight()+" Четверть: "+ rec.getQuadrant()+ " Площадь: "+rec.square());
                jojo[i]= rec;
            }
            System.out.println();
        }
    }
}


